# Yet Another Testing Framework

Why another testing framework? There are already excellent testing frameworks out there, it's difficult to improve the offer.
However there is some space for improvement...

What `yat` would like to achieve:

- being macro free for c++20 compliant compilation and almost macro-free for older versions
- having built-in support for BDD-style parsing
- having built-in support for mocks (with as little usage of macros as possible)
- supporting custom "operators" (e.g. "contains") allowing a high level of abstraction in assertions but being readable at the same time
- having reusable BDD clauses (`then() = it_succeeds;`, where `it_succeeds` is a previously defined object)
- allowing easy customization and extensions

## Examples

The snippet below is a trivial test that can be written with `yat`:

```
test_case test_1 = test("my first test") = []() {
    given("a precondition") = [](){
        const int a = 2;
        const int b = 3;
    
        when("something happens") = [&](){
            const int c = a + b;
        
            then("an effect is obtained") = [&](){
                require_that() << c == 5;
            };
        };
    };
};
```

a first advantage of using functions instead of macros can be seen with the snippet below:

```
test_case test_2 = test("the clauses are functions") = []() {
    auto the_sum_is_5 = [](int value){
        require() << value == 5;
    };

    when("adding 1 and 4") = [&](){
        const int c = 1 + 4;
    
        then("the sum is 5") = the_sum_is_5(c);
    };

    when("adding 2 and 3") = [&](){
        const int c = 2 + 3;
    
        then("the sum is 5") = the_sum_is_5(c);
    };
};
```

but the appetite comes with eating, and why not having (not yet implemented)

```
test_case test_2 = test("the clauses are functions") = []() {
    auto the_sum_is_5 = [](int value){
        require() << value == 5;
    };

    when("adding {} and {}") = [&](int a, int b){
        const int c = a + b;
    
        then("the sum is 5") = the_sum_is_5(c);
    } | generate({1,4}, {2,3});
};
```

or even (not yet implemented)

```
test_case test_2 = test("the clauses are functions") = []() {
    auto the_sum_is_5 = [](int value){
        require() << value == 5;
    };

    when("adding {} and {}") = [&](int a, int b){
        const int c = a + b;

        then("it is 5") = the_sum_is_5;
        
        then("it satisfies the requirements") = [](){
            require_that() << c =is_prime;
            require_that() << c =is_in= {4,5,6};
        };
    } | generate({1,4}, {2,3});
};
```

The idea is that reasoning (and programming) with function is simpler, and having a framework based on them can give more space to the immagination when writing tests.
