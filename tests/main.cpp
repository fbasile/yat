#include "yat/yat.hpp"

#include <iostream>

struct CoutBackend : StackResultStorage::Backend {
public:
    void add_result(Failure failure) override {
        std::cout << "---------------------------------------" << std::endl;
        std::cout << "In: " << failure.call_stack.back().location << std::endl << std::endl;
        for (const auto& location : failure.call_stack) {
            std::cout << location.description << std::endl;
        }
        std::cout << std::endl
                  << "Failure: " << failure.details.message << std::endl
                  << "---------------------------------------" << std::endl;
    }
};


int main() {
    CoutBackend backend;
    StackResultStorage storage(backend);
    TestRegister::instance().run(storage);

    std::cout << "Executed " << storage.test_cases().total << " test cases";
    if (storage.test_cases().failures) {
        std::cout << ", failures " << storage.test_cases().failures;
    }
    std::cout << std::endl;

    std::cout << "Executed " << storage.assertions.total << " assertions";
    if (storage.assertions.failures) {
        std::cout << ", failures " << storage.assertions.failures;
    }
    std::cout << std::endl;
}
