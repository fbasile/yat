#include "yat/yat.hpp"

namespace {

test_case test_1 = test("assertions") = []() {
    StackResultStorage result_storage;
    TestRegister::ScopedCollection tests;

    auto the_test_fails = [&] {
        tests.run(result_storage);

        then("the test is reported as failed with one failed assertion") = [&] {
            require_that() << result_storage.assertions.total == 1;
            require_that() << result_storage.assertions.failures == 1;
        };
    };

    auto the_test_succeeds = [&]() {
        tests.run(result_storage);

        then("the test is reported as succeeded with no failed assertions") = [&] {
            require_that() << result_storage.assertions.total == 1;
            require_that() << result_storage.assertions.failures == 0;
        };
    };


    given("a test with a == requirement that is going to fail") = [&] {
        test("my first test") = []() { require_that() << 1 == 2; };

        when("executing the test") = the_test_fails;
    };

    given("a test with a == requirement that is going to succeed") = [&] {
        test("my first test") = []() { require_that() << 1 == 1; };

        when("executing the test") = the_test_succeeds;
    };

    given("a test with two requirements that are going to fail") = [&] {
        test("my first test") = []() {
            require_that() << 1 == 2;
            require_that() << 1 == 3;
        };

        when("executing the test") = [&] {
            tests.run(result_storage);

            then("the test is reported as failed with only the first requirement as failed") = [&] {
                require_that() << result_storage.assertions.total == 1;
                require_that() << result_storage.assertions.failures == 1;
            };
        };
    };

    given("a test with two checks that are going to fail") = [&] {
        test("my first test") = []() {
            check_that() << 1 == 2;
            check_that() << 1 == 3;
        };

        when("executing the test") = [&] {
            tests.run(result_storage);

            then("the test is reported as failed with both the requirements marked as failed") = [&] {
                require_that() << result_storage.test_cases().total == 1;
                require_that() << result_storage.test_cases().failures == 1;
                require_that() << result_storage.assertions.total == 2;
                require_that() << result_storage.assertions.failures == 2;
            };
        };
    };


    given("boolean predicates") = [&] {
        struct IsOdd {
            int value;
            operator bool() const { return value % 2; }
        };

        given("a test with a boolean requirement that is going to succeed") = [&] {
            test("my first test") = []() { require() << IsOdd{3}; };

            when("executing the test") = the_test_succeeds;
        };

        given("a test with two boolean requirements that are going to fail") = [&] {
            test("my first test") = []() {
                require() << IsOdd{4};
                require() << IsOdd{6};
            };

            when("executing the test") = [&] {
                tests.run(result_storage);

                then("the test is reported as failed with only the first requirement as failed") = [&] {
                    require_that() << result_storage.assertions.total == 1;
                    require_that() << result_storage.assertions.failures == 1;
                };
            };
        };

        given("a test with two boolean checks that are going to fail") = [&] {
            test("my first test") = []() {
                check() << IsOdd{4};
                check() << IsOdd{6};
            };

            when("executing the test") = [&] {
                tests.run(result_storage);

                then("the test is reported as failed with both the requirements marked as failed") = [&] {
                    require_that() << result_storage.test_cases().total == 1;
                    require_that() << result_storage.test_cases().failures == 1;
                    require_that() << result_storage.assertions.total == 2;
                    require_that() << result_storage.assertions.failures == 2;
                };
            };
        };
    };


    given("a test with two failing independent clauses") = [&] {
        test("my first test") = []() {
            given("a condition") = []() {
                when("something happen") = []() {
                    then("the first fails") = []() { check_that() << 1 == 2; };
                    then("the second fails") = []() { check_that() << 1 == 3; };
                };
            };
        };

        when("executing the test") = [&] {
            tests.run(result_storage);

            then("the failures share part of the stack") = [&] {
                require_that() << result_storage.failures.size() == 2;
                require_that() << result_storage.failures[0].call_stack.size() == 4;
                require_that() << result_storage.failures[1].call_stack.size() == 4;
                for (int i = 0; i < 3; ++i) {
                    check_that() << result_storage.failures[0].call_stack[i].location
                        == result_storage.failures[1].call_stack[i].location;
                }
                check_that() << result_storage.failures[0].call_stack[3].location
                    != result_storage.failures[1].call_stack[3].location;
                require_that() << result_storage.assertions.total == 2;
                require_that() << result_storage.assertions.failures == 2;
            };
        };
    };

    given("a type that can be serialized") = [&]() {
        test("failing test") = [&]() { require_that() << 1 == 2; };

        when("an assertion fails") = [&]() {
            tests.run(result_storage);

            then("the values are serialized in the error") = [&]() {
                require_that() << result_storage.failures.size() == 1;
                require_that() << result_storage.failures[0].details.failed_assertion == "Failed assertion: 1 == 2";
            };
        };
    };

    given("a type that can't be serialized") = [&]() {
        struct A {
            int i;
            bool operator==(const A& that) const { return i == that.i; }
        };
        test("failing test") = [&]() { //
            require_that() << A{1} == A{2};
        };

        when("an assertion fails") = [&]() {
            tests.run(result_storage);

            then("the values are replace with a ? in the error") = [&]() {
                require_that() << result_storage.failures.size() == 1;
                require_that() << result_storage.failures[0].details.failed_assertion == "Failed assertion: {?} == {?}";
            };
        };
    };
};


}
