#include "yat/yat.hpp"

namespace {

test_case test_1 = test("registering tests") = []() {
    TestRegister::ScopedCollection scoped;

    test_case test1 = test("my first test") = []() {};

    require_that() << scoped.test_cases().size() == 1;
};

test_case test_2 = test("executing the tests") = []() {
    StackResultStorage result_storage;
    {
        TestRegister::ScopedCollection tests;

        test_case test1 = test("a test") = []() {};
        test_case test2 = test("another test") = []() {};
        tests.run(result_storage);
    }
    require_that() << result_storage.test_cases().total == 2;
    require_that() << result_storage.test_cases().failures == 0;
};
test_case test_3 = test("executing the tests with subsections inside") = []() {
    given("a test case with 2 clauses") = [&]() {
        TestRegister::TestCasesCollection tests;
        std::vector<int> receipts;
        {
            TestRegister::SwapCollections swap(tests);
            test("a test") = [&receipts]() {
                when("I encounter the first clause") = [&receipts]() {
                    receipts.push_back(1);
                    require() << false;
                };
                when("I encounter another clause") = [&receipts]() {
                    receipts.push_back(2);
                    require() << true;
                };
            };
        }
        when("executing the tests") = [&]() {
            StackResultStorage result_storage;
            tests.run(result_storage);
            then("all the subsections have been executed only once") = [&]() {
                require_that() << result_storage.test_cases().total == 1;
                require_that() << result_storage.test_cases().failures == 1;
                require_that() << result_storage.assertions.total == 2;
                require_that() << result_storage.assertions.failures == 1;
                require_that() << receipts.size() == 2;
                require_that() << std::set(receipts.begin(), receipts.end()).size() == 2;
            };
        };
    };

    given("a test case with 2 clauses and furtherly nested branches") = [&]() {
        TestRegister::TestCasesCollection tests;
        std::vector<int> receipts;
        {
            TestRegister::SwapCollections swap(tests);
            test("a test") = [&]() {
                given("something") = [&]() {
                    when("something happens") = [&]() {
                        then("I can check my assertions") = [&]() {
                            receipts.push_back(1);
                            require() << false;
                        };
                    };
                    when("something else happens") = [&]() {
                        then("I can check my assertions") = [&]() {
                            receipts.push_back(2);
                            require() << false;
                        };
                        then("I can check other assertions") = [&]() {
                            receipts.push_back(3);
                            require() << true;
                        };
                    };
                };
                given("something different") = [&]() {
                    receipts.push_back(4);
                    require() << true;
                };
            };
        }
        when("executing the tests") = [&]() {
            StackResultStorage result_storage;
            tests.run(result_storage);
            then("all the subsections have been executed only once") = [&]() {
                require_that() << result_storage.test_cases().total == 1;
                require_that() << result_storage.test_cases().failures == 1;
                require_that() << result_storage.assertions.total == 4;
                require_that() << result_storage.assertions.failures == 2;
                require_that() << receipts.size() == 4;
                require_that() << std::set(receipts.begin(), receipts.end()).size() == 4;
            };
        };
    };
};

} // namespace
