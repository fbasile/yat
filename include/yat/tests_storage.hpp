#pragma once

#include <cassert>
#include <cstring>
#include <experimental/source_location>
#include <functional>
#include <map>
#include <ostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

struct SourceLocation {
    SourceLocation(std::experimental::source_location location)
        : file(location.file_name()), line_number(location.line()) {}

    friend std::ostream& operator<<(std::ostream& out, const SourceLocation& location) {
        return out << location.file << ":" << location.line_number;
    }

    friend bool operator==(const SourceLocation& lhs, const SourceLocation& rhs) {
        return std::strcmp(lhs.file, rhs.file) == 0 and (lhs.line_number == rhs.line_number);
    }
    friend bool operator!=(const SourceLocation& lhs, const SourceLocation& rhs) { return !(lhs == rhs); }

private:
    const char* file;
    const int line_number;
};

struct Location {
    std::string description;
    SourceLocation location;
};

class ResultStorage {
public:
    struct InterruptExecution {
        bool top_most_scope = true;
    };

    struct Success {};
    struct Failure {
        std::string failed_assertion;
        std::string message;
        Location assertion_location;
    };

    virtual ~ResultStorage() = default;
    virtual void add_result(Success) = 0;
    virtual void add_result(Failure) = 0;
    virtual void enter_scope(Location) = 0;
    virtual void leave_scope() = 0;
    virtual void register_scope(Location, std::function<void()>) = 0;
};

struct StackResultStorage : ResultStorage {
public:
    struct Backend {
        using Success = ResultStorage::Success;
        struct Failure {
            ResultStorage::Failure details;
            std::vector<Location> call_stack;
        };

        virtual ~Backend() = default;
        virtual void add_result(Failure) = 0;
    };


    StackResultStorage() : stack_(Location{"fixme", std::experimental::source_location::current()}, root_) {}
    StackResultStorage(Backend& backend)
        : backend_(&backend), stack_(Location{"fixme", std::experimental::source_location::current()}, root_) {}

    struct Counter {
        size_t total = 0;
        size_t failures = 0;
    };

    void enter_scope(Location location) override {
        stack_.push(location, stack_.back().subscopes[location.description]);
    }
    void leave_scope() override { stack_.pop(); }
    void add_result(Success success) override { ++assertions.total; }
    void add_result(ResultStorage::Failure failure) override {
        ++assertions.total;
        ++assertions.failures;
        stack_.mark_failed();
        Backend::Failure failure_with_location{failure, stack_.location_stack()};
        failures.push_back(failure_with_location);
        if (backend_) {
            backend_->add_result(std::move(failure_with_location));
        }
    }

    void register_scope(Location location, std::function<void()> function) override {
        stack_.back().visit(stack_, *this, std::move(location), std::move(function));
    }

    std::vector<Backend::Failure> failures; // TODO move to another class
    Counter assertions;
    Counter test_cases() const {
        return {root_.subscopes.size(),
                static_cast<size_t>(std::count_if(root_.subscopes.begin(),
                                                  root_.subscopes.end(),
                                                  [](const auto& scope) { return scope.second.failed; }))};
    }
    // private:
    struct Scope;

    class Stack {
    public:
        Stack(const Location& location_root, Scope& root)
            : stack_({std::make_pair(std::ref(location_root), std::ref(root))}) {}
        void push(const Location& location, Scope& scope) { stack_.push_back({std::ref(location), std::ref(scope)}); }
        void pop() {
            stack_.pop_back();
            unwinding_ = stack_.size() > 1;
        }
        void mark_failed() {
            for (auto& scope : stack_) {
                scope.second.get().failed = true;
            }
        }
        bool is_unwinding() const { return unwinding_; }

        Scope& back() {
            assert(not stack_.empty());
            return stack_.back().second.get();
        }

        std::vector<Location> location_stack() const {
            std::vector<Location> res;
            res.reserve(stack_.size());
            for (size_t i = 1; i < stack_.size(); ++i) {
                res.push_back(stack_[i].first);
            }
            return res;
        }

    private:
        //         std::vector<std::pair<std::reference_wrapper<const Location>, std::reference_wrapper<Scope>>> stack_;
        std::vector<std::pair<Location, std::reference_wrapper<Scope>>> stack_;
        bool unwinding_ = false;
    };

    struct Scope {
    public:
        Scope() {}
        Scope(Scope&&) = default;
        Scope& operator=(Scope&&) = default;

        bool visit(Stack& stack, ResultStorage& result_storage, Location location, std::function<void()> function) {
            struct RAII {
                RAII(Stack& stack, ResultStorage& result_storage, const Location& location, Scope& subscope)
                    : stack(stack), result_storage(result_storage) {
                    result_storage.enter_scope(location);
                    //                     stack.push(location, subscope);
                }
                ~RAII() {
                    //                     stack.pop();
                    result_storage.leave_scope();
                }

                Stack& stack;
                ResultStorage& result_storage;
            };

            auto& subscope = subscopes[location.description];
            if (stack.is_unwinding()) {
                return false;
            }

            if (not subscope.fully_visited()) {
                RAII enter_scope(stack, result_storage, location, subscope);
                try {
                    function();
                } catch (const ResultStorage::InterruptExecution& interrupted) {
                    if (interrupted.top_most_scope) {
                        subscope.visited = true;
                    }
                    throw ResultStorage::InterruptExecution{false};
                }
                if (subscopes.empty()
                    or std::all_of(subscope.subscopes.begin(), subscope.subscopes.end(), [](const auto& subscope) {
                           return subscope.second.fully_visited(); // TODO add logic for exceptions
                       })) {
                    subscope.visited = true;
                }
            }
            return subscope.visited;
        }
        bool failed = false;
        bool fully_visited() const { return visited; }
        std::map<std::string, Scope> subscopes;

    private:
        bool visited = false;
    };

    bool visited = false;
    Scope root_;
    Stack stack_;
    Backend* const backend_ = nullptr;
};


class TestCase {
public:
    TestCase(Location&& location, std::function<void()>&& function)
        : location_(std::move(location)), function_(std::move(function)) {}

    void run(ResultStorage& result_storage) const { function_(); }

    const Location& location() const { return location_; }
    const std::function<void()>& function() const { return function_; }

private:
    const Location location_;
    const std::function<void()> function_;
};

class TestRegister {
public:
    /// \brief contains test cases
    class TestCasesCollection {
    public:
        void run(StackResultStorage& result_storage) {
            TestRegister::ScopedResultStorage results(result_storage);
            for (const auto& test : test_cases_) {
                bool completed = false;
                while (not completed) {
                    try {
                        completed = result_storage.root_.visit(result_storage.stack_,
                                                               result_storage,
                                                               test.location(),
                                                               test.function());
                    } catch (const ResultStorage::InterruptExecution& interrupt) {
                        if (interrupt.top_most_scope) {
                            completed = true;
                        }
                    }
                }
            }
        }
        void add(TestCase&& test_case) { test_cases_.push_back(std::move(test_case)); }
        const std::vector<TestCase>& test_cases() const { return test_cases_; }

    private:
        std::vector<TestCase> test_cases_;
    };

    /// \brief RAII object to swap the test register on the singleton
    class SwapCollections {
    public:
        SwapCollections(TestCasesCollection& collection) : previous_(TestRegister::instance().swap(collection)) {}
        ~SwapCollections() { TestRegister::instance().swap(previous_); }
        SwapCollections& operator=(const SwapCollections&) = delete;
        SwapCollections(const SwapCollections&) = delete;

    private:
        TestCasesCollection& previous_;
    };

    /// \brief RAII collection of test cases
    /// All the new test cases created in the scope where this class is present will
    /// be registered in this class.
    class ScopedCollection : public TestCasesCollection {
    public:
        ScopedCollection() : swap_(*this) {}

    private:
        SwapCollections swap_;
    };

    class ScopedResultStorage {
    public:
        ScopedResultStorage(StackResultStorage& storage) : previous_(TestRegister::instance().swap(&storage)) {}

        ~ScopedResultStorage() { TestRegister::instance().swap(previous_); }

    private:
        StackResultStorage* previous_;
    };

    static TestRegister& instance() {
        static TestRegister result;
        return result;
    }

    void add(TestCase&& test_case) { current_->add(std::move(test_case)); }

    void run(StackResultStorage& result_storage) const { current_->run(result_storage); }

    ResultStorage* current_result_storage() { return result_storage_; }
    const ResultStorage* current_result_storage() const { return result_storage_; }

private:
    StackResultStorage* swap(StackResultStorage* new_value) {
        auto buffer = new_value;
        std::swap(buffer, result_storage_);
        return buffer;
    }

    TestCasesCollection& swap(TestCasesCollection& new_value) {
        auto buffer = &new_value;
        std::swap(buffer, current_);
        return *buffer;
    }

    TestRegister() : current_(&root_) {}

    TestCasesCollection root_;
    TestCasesCollection* current_;
    StackResultStorage* result_storage_ = nullptr;
};
