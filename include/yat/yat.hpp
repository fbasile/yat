#pragma once

#include "yat/assertions.hpp"
#include "yat/tests_storage.hpp"

#include <cassert>
#include <experimental/source_location>
#include <functional>
#include <string>
#include <vector>

class NewTestCase {
public:
    NewTestCase(Location location, std::function<void()> function)
        : location_(std::move(location)), function_(std::move(function)) {}

    ~NewTestCase() { TestRegister::instance().add({std::move(location_), std::move(function_)}); }

private:
    Location location_;
    std::function<void()> function_;
};


// TODO rename
class TestRegistration {
public:
    TestRegistration(Location location) : location_(std::move(location)) {}

    NewTestCase operator=(std::function<void()> function) { // TODO && ?
        return NewTestCase(std::move(location_), std::move(function));
    }

private:
    Location location_;
};

/**
 * Build a test case:
 * \code
 * test_cast case_1 = test("My test") = [](){
 *   ...
 * };
 * \endcode
 */
inline TestRegistration test(const std::string& name,
                             std::experimental::source_location location
                             = std::experimental::source_location::current()) {
    return TestRegistration({"Test case: " + name, location});
}

/// Holder that allows to build static objects
struct test_case {
    constexpr test_case(const NewTestCase&) {}
};

class SectionRegistrer {
public:
    explicit SectionRegistrer(Location location) : location_(std::move(location)) {}

    void operator=(std::function<void()> function) {
        auto storage = TestRegister::instance().current_result_storage();
        if (storage) {
            storage->register_scope(location_, std::move(function));
        }
    }

private:
    Location location_;
};

inline SectionRegistrer given(const std::string& name,
                              std::experimental::source_location location
                              = std::experimental::source_location::current()) {
    return SectionRegistrer({"  Given: " + name, location});
}
inline SectionRegistrer when(const std::string& name,
                             std::experimental::source_location location
                             = std::experimental::source_location::current()) {
    return SectionRegistrer({"   When: " + name, location});
}
inline SectionRegistrer then(const std::string& name,
                             std::experimental::source_location location
                             = std::experimental::source_location::current()) {
    return SectionRegistrer({"   Then: " + name, location});
}
