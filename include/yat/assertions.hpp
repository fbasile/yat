#pragma once

#include "yat/tests_storage.hpp"

#include <experimental/source_location>
#include <sstream>
#include <utility>

enum class Policy { Require, Check };

namespace details {
template <class T>
auto serialize(std::ostream& out, const T& a) -> decltype(static_cast<std::ostream&>(out << a)) {
    return out << a;
}
template <class T>
std::ostream& serialize(std::ostream& out, ...) {
    return out << "{?}";
}
} // namespace details

inline void register_error(std::string error_summary, const SourceLocation& location, const Policy policy) {
    std::stringstream msg;
    msg << error_summary << "\n\n";
    msg << "At line: " << location;
    auto storage = TestRegister::instance().current_result_storage();
    if (storage) {
        storage->add_result(ResultStorage::Failure{std::move(error_summary), msg.str(), {"", location}});
    }
    if (policy == Policy::Require) {
        throw ResultStorage::InterruptExecution();
    }
}

struct LocatedAssertionFactory {
    LocatedAssertionFactory(SourceLocation location, Policy policy) : location_(std::move(location)), policy_(policy) {}

    template <class T>
    struct Member {
        Member(const SourceLocation& location, const T& value, Policy policy)
            : location_(location), value_(value), policy_(policy) {}

        ~Member() {
            if (not used_member_) {
                std::stringstream msg;
                msg << "Unmatched member in binary operator: ";
                details::serialize<T>(msg, value_);
                register_error(msg.str(), location_, Policy::Check);
            }
        }

        template <class V>
        void operator<(const V& that) {
            binary_comparison([](const auto& lhs, const auto& rhs) { return lhs < rhs; }, that, " < ");
        }
        template <class V>
        void operator<=(const V& that) {
            binary_comparison([](const auto& lhs, const auto& rhs) { return lhs <= rhs; }, that, " <= ");
        }
        template <class V>
        void operator>(const V& that) {
            binary_comparison([](const auto& lhs, const auto& rhs) { return lhs > rhs; }, that, " > ");
        }
        template <class V>
        void operator>=(const V& that) {
            binary_comparison([](const auto& lhs, const auto& rhs) { return lhs >= rhs; }, that, " >= ");
        }
        template <class V>
        void operator==(const V& that) {
            binary_comparison([](const auto& lhs, const auto& rhs) { return lhs == rhs; }, that, " == ");
        }
        template <class V>
        void operator!=(const V& that) {
            binary_comparison([](const auto& lhs, const auto& rhs) { return lhs != rhs; }, that, " != ");
        }

    private:
        template <class V, class Comparison>
        void binary_comparison(Comparison comparison, const V& that, const char* comparison_string) {
            used_member_ = true;
            if (comparison(value_, that)) {
                auto storage = TestRegister::instance().current_result_storage();
                if (storage) {
                    storage->add_result(ResultStorage::Success());
                }
            } else {
                fail(that, comparison_string);
            }
        }

        template <class V>
        void fail(const V& that, const char* operator_string) {
            std::stringstream msg;
            msg << "Failed assertion: ";
            details::serialize<T>(msg, value_);
            msg << operator_string;
            details::serialize<T>(msg, that);
            register_error(msg.str(), location_, policy_);
        }

        const SourceLocation& location_;
        const T& value_;
        const Policy policy_;
        bool used_member_ = false;
    };


    template <class V>
    Member<V> operator<<(const V& value) const {
        return Member<V>(location_, value, policy_);
    }

    const SourceLocation location_;
    const Policy policy_;
};

struct LocatedBooleanAssertionFactory {
    LocatedBooleanAssertionFactory(SourceLocation location, Policy policy)
        : location_(std::move(location)), policy_(policy) {}

    template <class T>
    struct Predicate {
        Predicate(const SourceLocation& location, const T& value, Policy policy) {
            if (value) {
                auto storage = TestRegister::instance().current_result_storage();
                if (storage) {
                    storage->add_result(ResultStorage::Success());
                }
            } else {
                std::stringstream msg;
                msg << "Failed assertion: " << value;
                register_error(msg.str(), location, policy);
            }
        }
    };


    template <class V>
    Predicate<V> operator<<(const V& value) const {
        return Predicate<V>(location_, value, policy_);
    }

private:
    const SourceLocation location_;
    const Policy policy_;
};


struct AssertionFactory {
    constexpr explicit AssertionFactory(Policy policy) : policy(policy) {}

    LocatedAssertionFactory operator()(std::experimental::source_location location
                                       = std::experimental::source_location::current()) const {
        return LocatedAssertionFactory(location, policy);
    }

private:
    const Policy policy;
};
struct BooleanAssertionFactory {
    constexpr explicit BooleanAssertionFactory(Policy policy) : policy(policy) {}

    LocatedBooleanAssertionFactory operator()(std::experimental::source_location location
                                              = std::experimental::source_location::current()) const {
        return LocatedBooleanAssertionFactory(location, policy);
    }

private:
    const Policy policy;
};


constexpr auto require_that = AssertionFactory(Policy::Require);
constexpr auto check_that = AssertionFactory(Policy::Check);
constexpr auto require = BooleanAssertionFactory(Policy::Require);
constexpr auto check = BooleanAssertionFactory(Policy::Check);
